<?php

	require_once $_SERVER['DOCUMENT_ROOT'] . '/api/includes/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/api/includes/db_actions.php';

	//парсим тельце запроса
	$postData = file_get_contents('php://input');
	$data = json_decode($postData, true);
	
	$api_key = !empty($data['api_key']) ? $data['api_key'] : NULL;
	$api_action = !empty($data['action']) ? $data['action'] : NULL;
	$user_data = !empty($data['user_data']) ? $data['user_data'] : NULL;
	
	//проверяем наличие API ключа
	if(!isset($api_key)){
		echo json_encode(
			array(
				'code' => 1,
				'message' => 'API key is required'
			)
		);

		die();
	}

	//проверяем API ключ
	if($api_key !== $config['api_key']){
		echo json_encode(
			array(
				'code' => 2,
				'message' => 'API key is invalid'
			)
		);

		die();
	}

	//проверяем название action
	if(!isset($api_action)){
		echo json_encode(
			array(
				'code' => 3,
				'message' => 'API action is required'
			)
		);

		die();
	}

	$db_actions = new DBActions($config);

	//проверяем существование action
	switch($api_action){
		case 'get_quantity_players':
			echo json_encode($db_actions->getCountPlayers());
		break;
		case 'get_player':
			if(empty($user_data)){
				echo json_encode(
					array(
						'code' => 5,
						'message' => 'User data is required'
					)
				);

				die();
			}

			echo json_encode($db_actions->getPlayer($user_data));
		break;
		case 'set_create_player':
			if(empty($user_data)){
				echo json_encode(
					array(
						'code' => 5,
						'message' => 'User data is required'
					)
				);

				die();
			}

			echo json_encode($db_actions->setCreatePlayer($user_data));
		break;
		case 'set_update_player':
			if(empty($user_data)){
				echo json_encode(
					array(
						'code' => 5,
						'message' => 'User data is required'
					)
				);

				die();
			}

			echo json_encode($db_actions->setUpdatePlayer($user_data));
		break;
		default:
			echo json_encode(
				array(
					'code' => 4,
					'message' => 'API action is invalid'
				)
			);

			die();
	}
?>