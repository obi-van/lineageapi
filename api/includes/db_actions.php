<?php
	class DBActions {

		private $dbaddress;
		private $dbuser;
		private $dbpass;
		private $dbname;
		private $worlddbname;

		public function __construct($config){
			$this->dbaddress = $config['dbaddress'];
			$this->dbuser = $config['dbuser'];
			$this->dbpass = $config['dbpass'];
			$this->dbname = $config['dbname'];
			$this->worlddbname = $config['worlddbname'];
		}

		public function gameDbConnect($dbName){
			$connect = mssql_connect($this->dbaddress, $this->dbuser, $this->dbpass);
			mssql_select_db($dbName, $connect);
			return $connect;
		}

		public function gameDbConnectClose($connect){
			mssql_close($connect);
		}

		//статистика по играющим
		public function getCountPlayers(){
			$countPlayers['online'] = 0;
			$countPlayers['sellers'] = 0;

			$connect = $this->gameDbConnect($this->worlddbname);
			if($connect){
				//online
				$result = mssql_query("SELECT char_id FROM dbo.user_data WHERE CAST(login AS DATETIME) > CAST(logout AS DATETIME) OR login IS NOT NULL AND logout IS NULL");
				$rows = mssql_num_rows($result);
				$countPlayers['online'] = $rows;

				//sellers - don't work on Advex HF
				$result = mssql_query("SELECT char_id FROM dbo.user_private_store WHERE end_time is NULL");
				$rows = mssql_num_rows($result);
				$countPlayers['sellers'] = $rows;

				$this->gameDbConnectClose($connect);
			}

			return $countPlayers;
		}

		public function getPlayer($user_data){
			$player['account'] = false;
			$player['email'] = false;

			$connect = $this->gameDbConnect($this->dbname);
			if($connect){
				//чекаем аккаунт
				$result = mssql_query(sprintf("SELECT account FROM dbo.user_account WHERE account = '%s' ", $user_data['account']));
				$rows = mssql_num_rows($result);
				if($rows > 0){
					$player['account'] = true;
				}

				//чекаем email
				$result = mssql_query(sprintf("SELECT email FROM dbo.ssn WHERE email = '%s' ", $user_data['email']));
				$rows = mssql_num_rows($result);
				if($rows > 0){
					$player['email'] = true;
				}

				$this->gameDbConnectClose($connect);
			}
			return $player;
		}

		public function setCreatePlayer($user_data){
			$question1 = '';
			$question2 = '';
			$ssn1 = mt_rand(1000000,9999999);
			$ssn2 = mt_rand(100000,999999);
			$ssn = $ssn1 . $ssn2;
			$password = $this->encrypt($user_data['key']);
			$answer1 = $this->encrypt($user_data['key']);
			$answer2 = $this->encrypt($user_data['key']);
			$account = $user_data['account'];
			$email = $user_data['email'];

			$connect = $this->gameDbConnect($this->dbname);
			if($connect){
				$sql = "INSERT INTO [ssn](ssn,name,email,job,phone,zip,addr_main,addr_etc,account_num) VALUES ('%s','%s','%s',0,'telphone','123456','','',1)";
				mssql_query(sprintf($sql, $ssn, $account, $email));

				$sql = "INSERT INTO user_account (account,pay_stat) VALUES ('%s', 1)";
				mssql_query(sprintf($sql, $account));

				$sql = "INSERT INTO user_info (account,ssn,kind) VALUES ('%s','%s', 99)";
				mssql_query(sprintf($sql, $account, $ssn));

				$sql = "INSERT INTO user_auth (account,password,quiz1,quiz2,answer1,answer2) VALUES ('%s',%s,'%s','%s',%s,%s)";
				mssql_query(sprintf($sql, $account, $password, $question1, $question2, $answer1, $answer2));

				$this->gameDbConnectClose($connect);
			}

			return array('result' => 'success');
		}

		public function setUpdatePlayer($user_data){
			$new_password = $this->encrypt($user_data['key']);
			$account = $user_data['account'];
			$connect = $this->gameDbConnect($this->dbname);
			if($connect){
				$sql = "UPDATE user_auth SET password = %s WHERE account = '%s' ";
				mssql_query(sprintf($sql, $new_password, $account));

				$this->gameDbConnectClose($connect);
			}

			return array('result' => 'success');
		}

		public function encrypt($str){
			$key = array();
			$dst = array();
			$i = 0;
			$nBytes = strlen($str);
			while ($i < $nBytes)
			{
				$i++;
				$key[$i] = ord(substr($str, $i - 1, 1));
				$dst[$i] = $key[$i];
			}
			$rslt = $key[1] + $key[2]*256 + $key[3]*65536 + $key[4]*16777216;
			$one = $rslt * 213119 + 2529077;
			$one = $one - intval($one/ 4294967296) * 4294967296;
	
			$rslt = $key[5] + $key[6]*256 + $key[7]*65536 + $key[8]*16777216;
			$two = $rslt * 213247 + 2529089;
			$two = $two - intval($two/ 4294967296) * 4294967296;
	
			$rslt = $key[9] + $key[10]*256 + $key[11]*65536 + $key[12]*16777216;
			$three = $rslt * 213203 + 2529589;
			$three = $three - intval($three/ 4294967296) * 4294967296;
	
			$rslt = $key[13] + $key[14]*256 + $key[15]*65536 + $key[16]*16777216;
			$four = $rslt * 213821 + 2529997;
			$four = $four - intval($four/ 4294967296) * 4294967296;
	
			$key[4] = intval($one/16777216);
			$key[3] = intval(($one - $key[4] * 16777216) / 65535);
			$key[2] = intval(($one - $key[4] * 16777216 - $key[3] * 65536) / 256);
			$key[1] = intval(($one - $key[4] * 16777216 - $key[3] * 65536 - $key[2] * 256));
	
			$key[8] = intval($two/16777216);
			$key[7] = intval(($two - $key[8] * 16777216) / 65535);
			$key[6] = intval(($two - $key[8] * 16777216 - $key[7] * 65536) / 256);
			$key[5] = intval(($two - $key[8] * 16777216 - $key[7] * 65536 - $key[6] * 256));
	
			$key[12] = intval($three/16777216);
			$key[11] = intval(($three - $key[12] * 16777216) / 65535);
			$key[10] = intval(($three - $key[12] * 16777216 - $key[11] * 65536) / 256);
			$key[9] = intval(($three - $key[12] * 16777216 - $key[11] * 65536 - $key[10] * 256));
	
			$key[16] = intval($four/16777216);
			$key[15] = intval(($four - $key[16] * 16777216) / 65535);
			$key[14] = intval(($four - $key[16] * 16777216 - $key[15] * 65536) / 256);
			$key[13] = intval(($four - $key[16] * 16777216 - $key[15] * 65536 - $key[14] * 256));
	
			$dst[1] = $dst[1] ^ $key[1];
	
			$i=1;
			while ($i<16)
			{
				$i++;
				$dst[$i] = $dst[$i] ^ $dst[$i-1] ^ $key[$i];
			}
	
			$i=0;
			while ($i<16)
			{
				$i++;
				if ($dst[$i] == 0)
				{
					$dst[$i] = 102;
				}
			}
	
			$encrypt = "0x";
			$i=0;
	
			while ($i<16)
			{
				$i++;
				if ($dst[$i] < 16)
				{
					$encrypt = $encrypt . "0" . dechex($dst[$i]);
				}
				else
				{
					$encrypt = $encrypt . dechex($dst[$i]);
				}
			}
			return $encrypt;
		}
	}
?>